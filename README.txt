connect the serial (microUSB port) which will show as like /dev/ttyUSB0. then you can run like "minicom -D /dev/ttyUSB0" to get a serial console. things should show up in the console when booting/etc.

if you power on the board normally (plug in 12V and press the red button) it will boot into android

connect USB-C with android booted to use adb to interact with the board (like "adb shell" to get a shell, "adb push ..." to push files, etc. may need to run "adb root" for root first)

you can backup things through adb - like "adb pull /dev/block/sde" which is 4GB and has those partitions you will be flashing over to use linux (boot_a, dtbo_a, vendor_boot_a, vbmeta_a)

for debian rootfs: https://wiki.debian.org/Arm64Port#Debootstrap_arm64
(and copy contents of firmware folder to /lib/firmware in your rootfs)

you can also flash your rootfs through adb if you are going to use the UFS storage for it. on the HDK "sdg/sdh" is 4GB each and completely unused by android. otherwise you can flash it to a USB drive which you will connected through the USB-C port (on on hub or whatever). using the m2 slot should also be possible but not all m2 drives are compatible and I need to enable support for it in the kernel

for kernel build clone this branch (https://github.com/flto/linux/commits/sm8550-6.6-el1). to build it cd into it and run:

	make O=../linux-build/ ARCH=arm64 CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- 8550_defconfig
	make O=../linux-build/ ARCH=arm64 CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- -j8

to boot into fastboot mode, either hold one of the volume buttons while powering on (don't remember which one) or "reboot bootloader" from android

to generate boot.img and vendor_boot.img from linux build:

python3 mkbootimg.py --header_version 4 --kernel $LINUX_BUILD/arch/arm64/boot/Image --dtb $LINUX_BUILD/arch/arm64/boot/dts/qcom/sm8550-mtp.dtb --cmdline "root=/dev/sdg rootwait pd_ignore_unused clk_ignore_unused" --pagesize 4096 --kernel_offset 0 --tags_offset 0x100 --base 0 -o boot.img --vendor_boot vendor_boot.img

Flashing:

fastboot flash boot_a boot.img
fastboot flash vendor_boot_a vendor_boot.img
fastboot flash dtbo_a dtbo_dummy

Might also need to reflash vbmeta_a with --disable-verification:

fastboot flash --disable-verification vbmeta_a vbmeta_a


